// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyAzgHsqxVBEtTrs86ZOwijjzgP478P95xQ',
        authDomain: 'wells-sonae-751b2.firebaseapp.com',
        databaseURL: 'https://wells-sonae-751b2.firebaseio.com',
        projectId: 'wells-sonae-751b2',
        storageBucket: 'wells-sonae-751b2.appspot.com',
        messagingSenderId: '764873828312',
        appId: '1:764873828312:web:9ab5e7fc8f3b1587'
    },
    miccrosoftConfig: {
        tenant: '51cbb4ff-b2b3-4610-ab83-53286edb17c3', prompt: 'consent'
    }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
