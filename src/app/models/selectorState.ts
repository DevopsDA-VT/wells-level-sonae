export interface State {
    dops?: boolean;
    stores?: boolean;
    aval?: boolean;
}

export interface Selected {
    dops?: string;
    stores?: string;
    aval?: string;
}

export interface Lists {
    dops: any[];
    stores: any[];
    aval: any[];
}

export class SelectorsState {
    state: State;
    selected: Selected;
    lists?: Lists;
}
