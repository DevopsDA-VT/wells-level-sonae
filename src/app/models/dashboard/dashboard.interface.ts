export interface DataState {
    loading?: boolean;
    disabled?: boolean;
    payload?: string[];
    selected?: string;
}

export interface DataTests {
    test1?: DataTest;
    test2?: DataTest;
    test3?: DataTest;
    test4?: DataTest;
    test5?: DataTest;
    createdAt?: any;
}

export interface DataTest {
    grades: number[];
    finalGrade: number;
    state: boolean;
}

export interface Dashboard {
    avals: DataState;
    dops: DataState;
    stores: DataState;
    collaborators: DataState;
    tests?: {
        payload: DataTests[],
        loading: boolean
    };
}

export interface ModalTestValues {
    tests: number[][];
}

export class DashboardInterface {
    avals: DataState;
    dops: DataState;
    stores: DataState;
    collaborators: DataState;
    tests?: {
        selected?: any;
        payload: DataTests[],
        loading: boolean
        disable: boolean
        complete: boolean
    };
    modal: ModalTestValues;


    constructor() {
        this.avals = {loading: false, disabled: true, selected: null};
        this.dops = {loading: true, disabled: true, selected: null};
        this.stores = {loading: false, disabled: true, selected: null};
        this.collaborators = {loading: false, disabled: true, selected: null};
        this.tests = {loading: false, disable: true, payload: [{}], complete: false};
        this.modal = {tests: [[], [], [], [], []]};
    }
}
