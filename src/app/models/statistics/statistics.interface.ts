import {DataState} from '../dashboard/dashboard.interface';

export interface Reviewer {
    displayName: string;
    rank: number;
}

export interface Graph {
    dop: any;
    store: any;
    storeperc: any;
}

export interface StatisticsInterface {
    dops: DataState;
    store: DataState;
    storeperc: DataState;
    reviewer: Reviewer;
    graph: Graph;
    collaborators: DataState;

}

export class StatisticsInterface {
    constructor() {
        this.dops = {loading: true, disabled: true, selected: null, payload: []};
        this.store = {loading: false, disabled: true, selected: null, payload: []};
        this.storeperc = {loading: false, disabled: true, selected: null, payload: []};
        this.collaborators = {loading: false, disabled: true, selected: null, payload: []};
        this.reviewer = {displayName: '', rank: 0};
        this.graph = {dop: [], store: [], storeperc: []};

    }
}
