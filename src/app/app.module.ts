import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NbAccordionModule, NbButtonModule, NbCardModule, NbDialogModule, NbIconModule, NbLayoutModule, NbListModule, NbMenuModule, NbRadioModule, NbSelectModule, NbSidebarModule, NbSpinnerModule, NbThemeModule} from '@nebular/theme';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {NgbActiveModal, NgbButtonsModule, NgbDatepickerModule, NgbModalModule, NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {FormWizardModule} from 'angular2-wizard';
import {ModalComponent} from './dashboard/modal/modal.component';
import {LoginComponent} from './auth/login/login.component';
import {NgxAuthFirebaseUIModule} from 'ngx-auth-firebaseui';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {MatIconModule} from '@angular/material';
import {LogoutComponent} from './auth/logout/logout.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {StoreModule} from '@ngrx/store';
import {DashboardReducer} from './dashboard/dashboard.reducer';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AngularFirePerformanceModule} from '@angular/fire/performance';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';
import {StatisticsReducer} from './statistics/statistics.reducer';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {UploadComponent} from './upload/upload.component';
import {StatisticsModalComponent} from './statistics/statistics-modal/statistics-modal.component';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import {NgxSpinnerModule} from 'ngx-spinner';

function firebaseAppNameFactory() {
    return environment.firebaseConfig.appId;
}


@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        ModalComponent,
        LoginComponent,
        LogoutComponent,
        StatisticsComponent,
        UploadComponent,
        StatisticsModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NbThemeModule.forRoot({name: 'corporate'}),
        NbLayoutModule,
        NbEvaIconsModule,
        NgbRatingModule,
        NgbDatepickerModule,
        FormsModule,
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbCardModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        NbSelectModule,
        NbListModule,
        NbIconModule,
        NbButtonModule,
        FormWizardModule,
        NbDialogModule.forRoot(),
        NgbModalModule,
        NgbButtonsModule,
        NbRadioModule,
        NbAccordionModule,
        NgxAuthFirebaseUIModule.forRoot(environment.firebaseConfig),
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule, // imports firebase/firestore, only needed for database features
        AngularFireAuthModule,
        MatIconModule,
        StoreModule.forRoot({dashboard: DashboardReducer, statistics: StatisticsReducer}),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
        NbSpinnerModule,
        AngularFirePerformanceModule,
        NgxChartsModule,
        SweetAlert2Module.forRoot(),
        NgxSpinnerModule

    ],
    providers: [AngularFireAuthGuard, NgbActiveModal],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [
        StatisticsModalComponent
    ]
})
export class AppModule {
}
