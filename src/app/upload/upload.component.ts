import {Component, OnInit} from '@angular/core';
import {UploadService} from '../services/upload.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

    constructor(private uploadService: UploadService, private spinner: NgxSpinnerService) {
    }

    ngOnInit() {

    }

    changeListener(files: FileList) {

        if (files && files.length > 0) {
            const file: File = files.item(0);

            if (file.type === 'text/csv') {
                const reader: FileReader = new FileReader();
                reader.readAsText(file);
                reader.onload = (e) => {
                    const csv: string = reader.result as string;
                    const csvArray: any[] = this.csvToArray(csv);

                    this.spinner.show();
                    this.uploadService.uploadToFireBase(csvArray).then(() => this.spinner.hide());

                };
            }

        }
    }

    private csvToArray(csvString) {
        const lines = csvString.split('\n');
        const headerValues = lines[0].split(';');
        const dataValues = lines.splice(1).map(dataLine => dataLine.split(';'));
        return dataValues.map(rowValues => {
            const row = {};
            headerValues.forEach((headerValue, index) => {
                row[headerValue] = (index < rowValues.length) ? rowValues[index] : null;
            });
            return row;
        });
    }
}
