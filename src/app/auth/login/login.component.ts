import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private auth: AuthService, private router: Router) {
    }

    ngOnInit() {
        this.auth.loginState().subscribe((state) => {
            if (state !== null) {
                this.router.navigate(['dashboard']).then();
            }
        });
    }

    LogInMicrosoft() {
        this.auth.login();
    }
}
