import {TestBed} from '@angular/core/testing';

import {DashboadModalService} from './dashboad-modal.service';

describe('DashboadModalService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: DashboadModalService = TestBed.get(DashboadModalService);
        expect(service).toBeTruthy();
    });
});
