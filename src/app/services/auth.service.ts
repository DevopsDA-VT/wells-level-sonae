import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    microsoftProvider = new firebase.auth.OAuthProvider('microsoft.com');

    constructor(private afAuth: AngularFireAuth, private router: Router, private afs: AngularFirestore) {
        this.microsoftProvider.setCustomParameters(environment.miccrosoftConfig);
    }


    login = () => {
        firebase.auth().signInWithPopup(this.microsoftProvider).then((response) => {

            this.afs.collection('Users').doc(this.afAuth.auth.currentUser.uid).valueChanges().subscribe((doc) => {
                if (!doc) {
                    const data = {
                        displayName: this.afAuth.auth.currentUser.displayName,
                        region: null,
                        email: this.afAuth.auth.currentUser.email,
                        createdAt: new Date(),
                        corporateId: null
                    };
                    this.afs.collection('Users').doc(this.afAuth.auth.currentUser.uid).set(data, {merge: true});
                }
            });
            this.router.navigate(['/dashboard']).then();
        });
    };

    loginState = () => this.afAuth.authState;

    getToken = () => this.afAuth.idToken;

    logout = () => this.afAuth.auth.signOut().then(() => {
        this.router.navigate(['auth/login']).then();
    });
}
