import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {AngularFirestore} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {SelectorsState} from '../models/selectorState';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    selectorsState: SelectorsState;
    dopsList: any = [];
    avalList: any = [];
    storeList: any = [];
    emploList: any = [];
    selectedEmplo: any;
    selectedDop: any;
    selectedAval: any;
    selectedStore: any;
    StoreRate: any = 0;
    LevelState: any = true;
    LevelTrueList: any = [];
    LevelFalseList: any = [];
    selectorData: any;

    constructor(private afs: AngularFirestore) {
    }

    getDops() {
        return this.afs.collection('Selectors').snapshotChanges().pipe(map(actions => {
            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    getAval(docID: string) {
        return this.afs.collection('Selectors').doc(docID).collection('Reviewers').snapshotChanges().pipe(map(actions => {
            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    getStore(DopId: string, AvalID: string) {
        return this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').snapshotChanges().pipe(map(actions => {
            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    getStoreRate = (collaborators: any) => {
        let sumRank = 0;
        let count = 0;
        try {

            for (const entry of collaborators) {

                if (entry.rank > 0 && entry.rank !== undefined && entry.rank !== null) {
                    count += 1;
                    sumRank += entry.rank;
                }
            }
            if (count === 0) {
                return 0;
            }
            return _.round((sumRank / count), 2);
        } catch (e) {

            return 0;
        }

    };
    getAvalRate = (stores: any) => {
        let sumRank = 0;
        let count = 0;
        try {
            for (const entry of stores) {
                if (entry.rank > 0 && entry.rank !== undefined && entry.rank !== null) {
                    count += 1;
                    sumRank += entry.rank;
                }
            }
            if (count === 0) {
                return 0;
            }
            return _.round((sumRank / count), 2);
        } catch (e) {

            return 0;
        }

    };
    getRegionRate = (Avals: any) => {
        let sumRank = 0;
        let count = 0;
        try {
            for (const entry of Avals) {
                if (entry.rank > 0 && entry.rank !== undefined && entry.rank !== null) {
                    count += 1;
                    sumRank += entry.rank;
                }
            }
            if (count === 0) {
                return 0;
            }
            return _.round((sumRank / count), 2);
        } catch (e) {

            return 0;
        }

    };

    getCollaborators(DopId: string, AvalID: string, StoreId: string) {
        return this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').snapshotChanges().pipe(map(actions => {

            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    getCollaboratorTests(DopId: string, AvalID: string, StoreId: string, CollabId: string) {

        return this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').doc(CollabId).collection('Tests').snapshotChanges().pipe(map(actions => {

            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    setTestData(DopId: string, AvalID: string, StoreId: string, CollabId: string, TestId: string, testValues: number[][]) {
        const data = {
            createdAt: new Date(),
            test1: {
                finalGrade: _.meanBy(testValues[0]),
                grades: testValues[0]
            },
            test2: {
                finalGrade: _.meanBy(testValues[1]),
                grades: testValues[1]
            },
            test3: {
                finalGrade: _.meanBy(testValues[2]),
                grades: testValues[2]
            },
            test4: {
                finalGrade: _.meanBy(testValues[3]),
                grades: testValues[3]
            },
            test5: {
                finalGrade: _.meanBy(testValues[4]),
                grades: testValues[4]
            }
        };
        if (TestId === undefined) {

            this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').doc(CollabId).collection('Tests')
                .add(data).then((response) => {


            });
        } else {
            this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').doc(CollabId).collection('Tests').doc(TestId)
                .set(data).then((response) => {


            });
        }
        this.afs
            .collection('Selectors')
            .doc(DopId)
            .collection('Reviewers')
            .doc(AvalID)
            .collection('Stores')
            .doc(StoreId)
            .collection('Collaborators')
            .doc(CollabId).set(data, {merge: true}).then();
    }

    updateCollaboratorRank(DopId: string, AvalID: string, StoreId: string, CollabId: string, rank: number) {
        const data = {
            rank
        };
        this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').doc(CollabId)
            .set(data, {merge: true}).then((response) => {
            this.updateStoreRank(DopId, AvalID, StoreId);

        });
    }

    updateStoreRank(DopId: string, AvalID: string, StoreId: string) {
        this.getCollaborators(DopId, AvalID, StoreId).subscribe((collaborators) => {
            const rate = this.getStoreRate(collaborators);

            const data = {
                rank: rate
            };
            this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId)
                .set(data, {merge: true}).then((response) => {
                this.updateAvalRank(DopId, AvalID);
            });
        });
    }

    updateAvalRank(DopId: string, AvalID: string) {
        this.getStore(DopId, AvalID).subscribe((stores: any) => {
            const rate = this.getAvalRate(stores);
            const data = {
                rank: rate
            };
            this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID)
                .set(data, {merge: true}).then((response) => {
                this.updateRegionRank(DopId);
            });
        });
    }

    updateRegionRank(DopId: string) {
        this.getAval(DopId).subscribe((avals: any) => {
            const rate = this.getAvalRate(avals);
            const data = {
                rank: rate
            };
            this.afs.collection('Selectors').doc(DopId)
                .set(data, {merge: true}).then((response) => {
            });
        });
    }
}
