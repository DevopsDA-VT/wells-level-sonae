import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {StatisticsInterface} from '../models/statistics/statistics.interface';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService {

    constructor(private afs: AngularFirestore, private store: Store<{ statistics: StatisticsInterface }>) {
    }

    getDops() {
        return this.afs.collection('Selectors').snapshotChanges().pipe(map(actions => {

            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    getDBStores(dopId: string) {
        return this.afs.collection('Selectors').doc(dopId).collection('Reviewers').snapshotChanges().pipe(map(reviewers => {
            const reviewerArray = reviewers.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
            return this.getStoresFromAvals(reviewerArray, dopId).then((storesArray) => storesArray);
        }));
    }

    getGlobalDopRank(Dops) {
        let sumRank = 0;
        let count = 0;

        try {
            for (const entry of Dops.payload) {
                if (entry.rank > 0 && entry.rank !== undefined && entry.rank !== null) {
                    count += 1;
                    sumRank += entry.rank;
                }
            }
            if (count === 0) {
                return 0;
            }
            return _.round((sumRank / count), 2);
        } catch (e) {
            console.log(e);
            return 0;
        }
    }

    getDopRank(stores) {
        let sumRank = 0;
        let count = 0;

        try {
            for (const entry of stores.payload) {
                if (entry.rank > 0 && entry.rank !== undefined && entry.rank !== null) {
                    count += 1;
                    sumRank += entry.rank;
                }
            }
            if (count === 0) {
                return 0;
            }
            return _.round((sumRank / count), 2);
        } catch (e) {
            console.log(e);
            return 0;
        }
    }

    getReviewer(dopId: any, parentId: any) {
        return this.afs.collection('Selectors').doc(dopId).collection('Reviewers').doc(parentId).valueChanges();
    }

    getCollaborator(dopID: string, parent: any, storeID: any) {
        return this.afs.collection('Selectors').doc(dopID).collection('Reviewers').doc(parent).collection('Stores').doc(storeID).collection('Collaborators').snapshotChanges().pipe(map(reviewers => {
            return reviewers.map(a => {

                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                const tests = this.afs.collection('Selectors').doc(dopID).collection('Reviewers').doc(parent).collection('Stores').doc(storeID).collection('Collaborators').doc(id).collection('Tests').valueChanges();
                return {id, tests, ...payload};
            });
        }));
    }

    getCollaboratorTests(DopId: string, AvalID: string, StoreId: string, CollabId: string) {

        return this.afs.collection('Selectors').doc(DopId).collection('Reviewers').doc(AvalID).collection('Stores').doc(StoreId).collection('Collaborators').doc(CollabId).collection('Tests').snapshotChanges().pipe(map(actions => {

            return actions.map(a => {
                const payload = a.payload.doc.data();
                const id = a.payload.doc.id;
                return {id, ...payload};
            });
        }));
    }

    private getStoresFromAvals(reviewerArray: any[], dopId: string) {
        const storesArray = [];
        for (let i = 0, len = reviewerArray.length; i < len; i++) {
            storesArray.push({parent: reviewerArray[i].id, data: this.afs.collection('Selectors').doc(dopId).collection('Reviewers').doc(reviewerArray[i].id).collection('Stores').get()});
        }
        return Promise.all(storesArray);
    }
}
