import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class UploadService {

    DopIds: any[] = [];

    constructor(private afs: AngularFirestore) {
    }

    async uploadToFireBase(csv: any) {

        for (const line of csv) {
            await this.delay(500);
            const data = {
                dop: line.DOP,
                reviewer: line.RT,
                store: line.UNIDADEDIRECAO,
                collaboratorId: line.ID,
                displayName: line.COLABORADOR,
                job: line.CARGO
            };

            this.checkCollaborator(data);
        }
    }

    private checkCollaborator(data) {
        console.log({checkingCollaborator: data});
        this.afs
            .collection('Collaborators', ref => ref.where('collaboratorId', '==', data.collaboratorId))
            .get()
            .subscribe((doc: any) => {

                if (doc.docs.length > 0) {

                    doc.docs.forEach((doc) => {

                        const docData = doc.data();

                        if (data.store !== docData.store) {

                            console.log('changed');

                        }
                    });

                } else {

                    this.addCollaborator(data).then((res) => {


                        if (this.DopIds[data.dop] === undefined) {

                            this.getDopId(data).subscribe((doc) => {

                                for (const entry of doc.docs) {

                                    this.DopIds[data.dop] = entry.id;
                                    this.checkReviewer(data, entry.id);
                                }
                            });
                        } else {

                            this.checkReviewer(data, this.DopIds[data.dop]);
                        }
                    });
                }
            });
    }

    private addCollaborator(data: any) {
        return this.afs
            .collection('Collaborators')
            .add(data);
    }

    private getDopId(data: any) {
        return this.afs
            .collection('Selectors', ref => ref.where('dop', '==', data.dop))
            .get();

    }

    private checkReviewer(data: any, id: string) {

        this.afs
            .collection('Selectors')
            .doc(id)
            .collection('Reviewers', ref => ref.where('displayName', '==', data.reviewer))
            .get()
            .subscribe((doc) => {

                if (doc.docs.length > 0) {
                    doc.docs.forEach((doc) => {
                        this.checkStore(data, id, doc.id);
                    });
                } else {

                    this.addReviewer(data, id).then((doc) => {
                        this.checkStore(data, id, doc.id);
                    });
                }
            });

    }

    private addReviewer(data: any, id: string) {
        const reviewerData = {
            displayName: data.reviewer,
            rank: null
        };

        return this.afs
            .collection('Selectors').doc(id).collection('Reviewers')
            .add(reviewerData);
    }

    private async checkStore(data: any, dopId: string, reviewerId: string) {
        await this.delay(1000);
        this.afs
            .collection('Selectors')
            .doc(dopId)
            .collection('Reviewers')
            .doc(reviewerId)
            .collection('Stores', ref => ref.where('displayName', '==', data.store))
            .get()
            .subscribe((doc) => {
                if (doc.docs.length > 0) {
                    doc.docs.forEach((doc) => {
                        this.checkStoreCollaborator(data, dopId, reviewerId, doc.id);
                    });
                } else {

                    this.addStore(data, dopId, reviewerId).then((doc) => {
                        this.checkStoreCollaborator(data, dopId, reviewerId, doc.id);
                    });
                }
            });
    }

    private async checkStoreCollaborator(data: any, dopId: string, reviewerId: string, storeId: string) {
        await this.delay(2000);
        this.afs
            .collection('Selectors')
            .doc(dopId)
            .collection('Reviewers')
            .doc(reviewerId)
            .collection('Stores')
            .doc(storeId)
            .collection('Collaborators', ref => ref.where('displayName', '==', data.displayName))
            .get()
            .subscribe((doc) => {
                if (doc.docs.length > 0) {

                } else {
                    this.addStoreCollaborator(data, dopId, reviewerId, storeId).then((res) => {
                        console.log(res);
                    });
                }
            });
    }

    private async addStore(data: any, dopId: string, reviewerId: string) {
        const StoreData = {
            displayName: data.store,
            rank: null
        };

        return this.afs
            .collection('Selectors')
            .doc(dopId)
            .collection('Reviewers')
            .doc(reviewerId)
            .collection('Stores')
            .add(StoreData);
    }

    private async addStoreCollaborator(data: any, dopId: string, reviewerId: string, storeId: string) {
        await this.delay(2000);
        const reviewerData = {
            displayName: data.displayName,
            collaboratorId: data.collaboratorId,
            rank: null,
            state: 1,
            job: data.job
        };

        return this.afs
            .collection('Selectors')
            .doc(dopId)
            .collection('Reviewers')
            .doc(reviewerId)
            .collection('Stores')
            .doc(storeId)
            .collection('Collaborators')
            .add(reviewerData);
    }

    private delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    //
    //   async uploadToFireBase(csv: any) {
    //
    //       for (const line of csv) {
    //         console.log({line});
    //         await this.delay(500);
    //         const data = {
    //               dop: line.DOP,
    //               reviewer: line.RT,
    //               store: line.UNIDADEDIRECAO,
    //               collaboratorId: line.ID,
    //               displayName: line.COLABORADOR,
    //               job: line.CARGO
    //           };
    //         this.afs.collection('Collaborators', ref => ref.where('collaboratorId', '==', data.collaboratorId)).get().subscribe((doc: any) => {
    //               if (doc.docs.length > 0) {
    //                   doc.docs.forEach((doc) => {
    //                       const docData = doc.data();
    //                       if (data.store !== docData.store) {
    //                           console.log('changed');
    //                       }
    //                   });
    //               } else {
    //                   this.afs.collection('Collaborators').add(data).then((res) => {
    //                       this.afs.collection('Selectors', ref => ref.where('dop', '==', data.dop)).get().subscribe((doc: any) => {
    //                           if (doc.docs.length > 0) {
    //                               doc.docs.forEach(async (doc) => {
    //                                   const dopId = doc.id;
    //
    //                                   this.afs.collection('Selectors').doc(dopId).collection('Reviewers', ref => ref.where('displayName', '==', data.reviewer)).get().subscribe((doc: any) => {
    //
    //                                       if (doc.docs.length > 0) {
    //                                           doc.docs.forEach((doc) => {
    //                                               const ReviewerId = doc.id;
    //                                               this.addStore(dopId, data, doc);
    //                                           });
    //                                       } else {
    //
    //                                           const reviewerData = {
    //                                               displayName: data.reviewer,
    //                                               rank: null
    //                                           };
    //                                           this.afs.collection('Selectors')
    //                                               .doc(dopId).collection('Reviewers')
    //                                               .add(reviewerData)
    //                                               .then(async (ref) => {
    //                                                   this.addStore(dopId, data, ref);
    //                                               });
    //                                       }
    //                                   });
    //                               });
    //                           }
    //                       });
    //                   });
    //               }
    //           });
    //       }
    //   }
    //

    //   addStore(dopId: any, data: any, ref: any) {
    //
    //     this.afs.collection('Selectors')
    //         .doc(dopId).collection('Reviewers')
    //         .doc(ref.id).collection('Stores', ref => ref.where('displayName', '==', data.store))
    //         .get()
    //         .subscribe((doc) => {
    //           if (doc.docs.length > 0) {
    //             doc.docs.forEach((doc) => {
    //               const StoreId = doc.id;
    //               console.log({doc: doc.data()});
    //               this.addColab(dopId, data, doc);
    //             });
    //           } else {
    //
    //             const StoreData = {
    //               displayName: data.store,
    //               rank: null
    //             };
    //             this.afs.collection('Selectors')
    //                 .doc(dopId).collection('Reviewers')
    //                 .doc(ref.id)
    //                 .collection('Stores')
    //                 .add(StoreData)
    //                 .then(async (ref) => {   this.addColab(dopId, data, ref);});
    //           }
    //         });
    //   }
    // addColab(dopId: any, data: any, ref: any) {
    //
    //   this.afs.collection('Selectors')
    //       .doc(dopId).collection('Reviewers')
    //       .doc(ref.id).collection('Stores', ref => ref.where('displayName', '==', data.store))
    //       .get()
    //       .subscribe((doc) => {
    //         const collabData =  {
    //           displayName: data.displayName,
    //           id: data.collaboratorId,
    //           state: 1,
    //           job: data.job,
    //           rank: null
    //         };
    //         if (doc.docs.length > 0) {
    //           doc.docs.forEach((doc) => {
    //             const StoreId = doc.id;
    //             this.afs.collection('Selectors')
    //                 .doc(dopId).collection('Reviewers')
    //                 .doc(ref.id).collection('Stores').doc(StoreId).collection('Collaborators').add(collabData).then();
    //
    //           });
    //         } else {
    //           this.afs.collection('Selectors')
    //                   .doc(dopId).collection('Reviewers')
    //                   .doc(ref.id).collection('Stores').doc(ref.id).collection('Collaborators').add(collabData).then();
    //
    //         }
    //       });
    // }


}
