import {Action} from '@ngrx/store';

export enum DashboardActionTypes {
    UpdateTestResults = '[Dashboard Component] update Results',
    DBDops = '[Dashboard Component] DB Get Dops',
    DBAvals = '[Dashboard Component] DB Get Avals',
    DBStores = '[Dashboard Component] DB Get Stores',
    DBCollaborators = '[Dashboard Component] DB Get Collaborators',
    DBCollaboratorTests = '[Dashboard Component] DB Get Collaborator Tests',
    SetDop = '[Dashboard Component] Set Selected Dop',
    SetAval = '[Dashboard Component] Set Selected Aval',
    SetStore = '[Dashboard Component] Set Selected Store',
    SetCollaborator = '[Dashboard Component] Set Selected Collaborator',
    SetCollaboratorsLoading = '[Dashboard Component] Set loading state to Collaborators',
    ModalSetTestResult = '[Dashboard Component] Set modal test state',
    UpdateTestState = '[Dashboard Component] Set modal test save state',
}

export class ActionEx implements Action {
    readonly type;
    payload: any;
}

export class DashboardDbDops implements ActionEx {
    readonly type = DashboardActionTypes.DBDops;

    constructor(public payload: any) {
    }
}

export class DashboardSetDop implements ActionEx {
    readonly type = DashboardActionTypes.SetDop;

    constructor(public payload: any) {
    }
}

export class DashboardDbAvals implements ActionEx {
    readonly type = DashboardActionTypes.DBAvals;

    constructor(public payload: any) {
    }
}

export class DashboardSetAval implements ActionEx {
    readonly type = DashboardActionTypes.SetAval;

    constructor(public payload: any) {
    }
}

export class DashboardDBStores implements ActionEx {
    readonly type = DashboardActionTypes.DBStores;

    constructor(public payload: any) {
    }
}

export class DashboardSetStore implements ActionEx {
    readonly type = DashboardActionTypes.SetStore;

    constructor(public payload: any) {
    }
}

export class DashboardDBCollaborators implements ActionEx {
    readonly type = DashboardActionTypes.DBCollaborators;

    constructor(public payload: any) {
    }
}

export class DashboardSetCollaboratorsLoading implements ActionEx {
    readonly type = DashboardActionTypes.SetCollaboratorsLoading;
    payload: any;

    constructor() {
    }
}

export class DashboardSetCollaborator implements ActionEx {
    readonly type = DashboardActionTypes.SetCollaborator;

    constructor(public payload: any) {
    }
}

export class DashboardDBCollaboratorTests implements ActionEx {
    readonly type = DashboardActionTypes.DBCollaboratorTests;

    constructor(public payload: any) {
    }
}

export class DashboardModalSetTestResult implements ActionEx {
    readonly type = DashboardActionTypes.ModalSetTestResult;

    constructor(public payload: any) {
    }
}

export class DashboardUpdateTestResults implements ActionEx {
    readonly type = DashboardActionTypes.UpdateTestResults;

    constructor(public payload: any) {
    }
}

export class DashboardModalSetTestState implements ActionEx {
    readonly type = DashboardActionTypes.UpdateTestState;

    constructor(public payload: any) {
    }
}
