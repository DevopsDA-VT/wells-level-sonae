import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DashboadModalService} from '../../services/dashboad-modal.service';
import {select, Store} from '@ngrx/store';
import {DashboardInterface} from '../../models/dashboard/dashboard.interface';
import {Observable} from 'rxjs';
import {DashboardModalSetTestResult, DashboardModalSetTestState} from '../dashboard.actions';

@Component({
    selector: 'app-modal-tab',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    @Input() options: any;
    @Input() level: any;
    @Input() subTests: any;
    @Input() testName: any;
    setChoice: any[] = [];
    dashboard: Observable<DashboardInterface>;
    tests: any;
    private testSize: number;

    constructor(private modalService: NgbModal, private dashModelService: DashboadModalService, private store: Store<{ dashboard: DashboardInterface }>) {
        this.dashboard = store.pipe(select('dashboard'));
        this.dashboard.subscribe((res: DashboardInterface) => {
            this.tests = res.modal.tests;
        });
    }

    ngOnInit() {
        this.store.dispatch(new DashboardModalSetTestState(false));
    }

    setOption(level: any, $event: any, index: number) {
        this.store.dispatch(new DashboardModalSetTestResult({level, $event, index}));
        this.dashboard.subscribe((res: DashboardInterface) => {
            this.testSize = res.modal.tests[level].length;
            this.checkSaveState(res);
        });


    }

    checkSaveState(res) {
        if (res.modal.tests[this.level].length === this.subTests[this.level].length) {
            this.store.dispatch(new DashboardModalSetTestState(true));
        }

    }

}
