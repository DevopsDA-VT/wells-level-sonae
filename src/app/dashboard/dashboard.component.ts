import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DashboardService} from '../services/dashboard.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {SubTestsData, TestsData} from '../../assets/data/tests';
import {
    DashboardDbAvals,
    DashboardDBCollaborators,
    DashboardDBCollaboratorTests,
    DashboardDbDops,
    DashboardDBStores,
    DashboardSetAval,
    DashboardSetCollaborator,
    DashboardSetCollaboratorsLoading,
    DashboardSetDop,
    DashboardSetStore,
    DashboardUpdateTestResults
} from './dashboard.actions';
import {DashboardInterface} from '../models/dashboard/dashboard.interface';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
    dashboard: Observable<DashboardInterface>;
    dashboardState: DashboardInterface = new DashboardInterface();

    width: any;
    options = [
        {value: 1, label: 1},
        {value: 2, label: 2},
        {value: 3, label: 3},
        {value: 4, label: 4},
        {value: 5, label: 5},
    ];
    Tests = [];
    SubTests = [];
    optionLevel: any[] = [];
    private closeResult: string;
    private storeRanking = 0;

    constructor(public dashService: DashboardService, private modalService: NgbModal, private store: Store<{ dashboard: DashboardInterface }>) {
        this.Tests = TestsData;
        this.SubTests = SubTestsData;
        this.width = window.innerWidth;
        for (const test of this.Tests) {
            this.optionLevel.push([]);
        }
        this.dashboard = store.pipe(select('dashboard'));
    }

    ngOnInit() {
        try {
            this.dashService.getDops().subscribe((querySnapshot) => {
                this.store.dispatch(new DashboardDbDops(querySnapshot.sort((a: any, b: any) => {
                    if (a.name < b.name) {
                        return -1;
                    }
                    if (a.name > b.name) {
                        return 1;
                    }
                    return 0;
                })));
            });
        } catch (e) {

        }

    }

    getDops(): string[] {
        this.dashboard.subscribe((res: DashboardInterface) => {
            this.dashboardState.dops = res.dops;
        });
        return this.dashboardState.dops.payload;
    }

    getDopsStateDisable(): Observable<boolean> {
        return this.store.select(state => state.dashboard.dops.disabled);
    }

    getDopsStateLoading(): Observable<boolean> {
        return this.store.select(state => state.dashboard.dops.loading);
    }

    setSelectedDop($event) {
        this.store.dispatch(new DashboardSetDop($event));
        this.getAvalsStore();
    }

    getAvalsStore() {
        this.dashService.getAval(this.dashboardState.dops.selected).subscribe((querySnapshot) => {
            this.store.dispatch(new DashboardDbAvals(querySnapshot.sort((a: any, b: any) => {
                if (a.displayName < b.displayName) {
                    return -1;
                }
                if (a.displayName > b.displayName) {
                    return 1;
                }
                return 0;
            })));
        });
    }

    getSelectedDop() {
        return this.store.select(state => state.dashboard.dops.selected);
    }

    getAvalsStateDisable(): Observable<boolean> {
        return this.store.select(state => state.dashboard.avals.disabled);
    }

    getAvalsStateLoading(): Observable<boolean> {
        return this.store.select(state => state.dashboard.avals.loading);
    }

    getAvals(): string[] {
        this.dashboard.subscribe((res: DashboardInterface) => {
            this.dashboardState.avals = res.avals;
        });
        return this.dashboardState.avals.payload;
    }

    setSelectAval($event) {
        this.store.dispatch(new DashboardSetAval($event));
        this.dashService.getStore(this.dashboardState.dops.selected, this.dashboardState.avals.selected).subscribe((querySnapshot) => {
            this.store.dispatch(new DashboardDBStores(querySnapshot.sort((a: any, b: any) => {
                if (a.displayName < b.displayName) {
                    return -1;
                }
                if (a.displayName > b.displayName) {
                    return 1;
                }
                return 0;
            })));
        });
    }

    getSelectedAval() {

        return this.store.select(state => state.dashboard.avals.selected);
    }

    getStoresStateDisable(): Observable<boolean> {
        return this.store.select(state => state.dashboard.stores.disabled);
    }

    getStoresStateLoading(): Observable<boolean> {
        return this.store.select(state => state.dashboard.stores.loading);
    }

    getStores() {
        this.dashboard.subscribe((res: DashboardInterface) => {
            this.dashboardState.stores = res.stores;
        });
        return this.dashboardState.stores.payload;
    }

    setSelectedStore($event) {

        this.store.dispatch(new DashboardSetStore($event));
        this.store.dispatch(new DashboardSetCollaboratorsLoading());
        this.dashService.getCollaborators(this.dashboardState.dops.selected, this.dashboardState.avals.selected, $event).subscribe((querySnapshot) => {
            this.store.dispatch(new DashboardSetCollaboratorsLoading());
            this.store.dispatch(new DashboardDBCollaborators(querySnapshot));
        });

    }

    getSelectedStore() {
        return this.store.select(state => state.dashboard.stores.selected);
    }

    getCollaborators() {

        let collabData = [];

        this.dashboard.subscribe((res: DashboardInterface) => {
            this.dashboardState.collaborators = res.collaborators;
        });
        if (this.dashboardState.collaborators.payload) {
            collabData = this.dashboardState.collaborators.payload.filter((payload: any) => payload.state !== '0').sort((a: any, b: any) => {
                if (a.displayName < b.displayName) {
                    return -1;
                }
                if (a.displayName > b.displayName) {
                    return 1;
                }
                return 0;
            });

        }

        return collabData;
    }

    setSelectedCollaborator(id: string) {
        this.store.dispatch(new DashboardSetCollaborator(id));
        this.dashService.getCollaboratorTests(this.dashboardState.dops.selected, this.dashboardState.avals.selected, this.dashboardState.stores.selected, id).subscribe((querySnapshot) => {

            this.store.dispatch(new DashboardDBCollaboratorTests(querySnapshot));
        });
    }

    getCollaboratorsState() {
        return this.store.select(state => state.dashboard.collaborators.disabled);
    }

    getCollaboratorTests() {

        this.dashboard.subscribe((res: DashboardInterface) => {

            this.dashboardState.tests = res.tests;
        });
        return [this.dashboardState.tests.payload[0]];
    }

    GetStoreRate() {
        if (this.dashboardState.stores.selected !== null && this.dashboardState.collaborators.disabled === false && this.dashboardState.dops.selected !== null && this.dashboardState.avals.selected !== null) {
            this.storeRanking = this.dashService.getStoreRate(this.dashboardState.collaborators.payload);
        } else {
            this.storeRanking = 0;
        }
        return this.storeRanking;
    }

    getCollaboratorsStateLoading() {
        return this.store.select(state => state.dashboard.collaborators.loading);
    }

    getCollaboratorsTestLoading() {
        return this.store.select(state => state.dashboard.tests.loading);
    }

    getCollaboratorState(index: number) {
        return this.store.select(state => {
            return (state.dashboard.tests.disable);
        });
    }


    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getDismissReason = (reason: any): string => {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    };

    checkLevelState(number: number) {
        return false;
    }

    saveTestResult(modal: any) {
        try {
            this.dashboard.subscribe((res: DashboardInterface) => {
                this.dashService.setTestData(
                    this.dashboardState.dops.selected,
                    this.dashboardState.avals.selected,
                    this.dashboardState.stores.selected,
                    this.dashboardState.collaborators.selected,
                    this.dashboardState.tests.selected,
                    res.modal.tests);
                this.store.dispatch(new DashboardUpdateTestResults(null));
                this.dashService.getCollaboratorTests(this.dashboardState.dops.selected, this.dashboardState.avals.selected, this.dashboardState.stores.selected, this.dashboardState.collaborators.selected).subscribe((querySnapshot) => {

                    this.store.dispatch(new DashboardDBCollaboratorTests(querySnapshot));
                    this.store.select(state => state.dashboard.tests.payload[0]).subscribe((response) => {
                        if (response.test1.finalGrade !== null &&
                            response.test2.finalGrade !== null &&
                            response.test3.finalGrade !== null &&
                            response.test4.finalGrade !== null &&
                            response.test5.finalGrade !== null &&
                            !Number.isNaN(response.test1.finalGrade) &&
                            !Number.isNaN(response.test2.finalGrade) &&
                            !Number.isNaN(response.test3.finalGrade) &&
                            !Number.isNaN(response.test4.finalGrade) &&
                            !Number.isNaN(response.test5.finalGrade)) {
                            let count = 0;
                            if (response.test1.finalGrade >= 3) {
                                count++;
                            }
                            if (response.test2.finalGrade >= 3) {
                                count++;
                            }

                            if (response.test3.finalGrade >= 3) {
                                count++;
                            }

                            if (response.test4.finalGrade >= 3) {
                                count++;
                            }

                            if (response.test5.finalGrade >= 3) {
                                count++;
                            }
                            this.dashService.updateCollaboratorRank(this.dashboardState.dops.selected, this.dashboardState.avals.selected, this.dashboardState.stores.selected, this.dashboardState.collaborators.selected, count);
                            this.dashService.getCollaboratorTests(this.dashboardState.dops.selected, this.dashboardState.avals.selected, this.dashboardState.stores.selected, this.dashboardState.collaborators.selected).subscribe((querySnapshot) => {

                                this.store.dispatch(new DashboardDBCollaboratorTests(querySnapshot));
                            });
                        }
                    });
                });


                modal.close();

            });
        } catch (e) {

        }
    }

    getTestSaveState() {
        // try {
        console.log(this.dashboardState.tests.complete);
        return !this.dashboardState.tests.complete;
        //     this.dashboard.subscribe((res: DashboardInterface) => {
        //
        //         this.dashboardState.tests.complete = !res.tests.complete;
        //     });
        //     return this.dashboardState.tests.complete;
        // } catch (e) {
        //     console.log(e);
        // }

    }
}
