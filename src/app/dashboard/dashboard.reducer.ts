import {ActionEx, DashboardActionTypes} from './dashboard.actions';
import {DashboardInterface} from '../models/dashboard/dashboard.interface';

export const initialState = new DashboardInterface()
;


export function DashboardReducer(state: DashboardInterface = initialState, action: ActionEx) {
    switch (action.type) {
        case DashboardActionTypes.DBDops:

            // change dops state

            state.dops.loading = false;
            state.dops.disabled = false;
            state.dops.payload = action.payload;


            return state;

        case DashboardActionTypes.DBAvals:

            state.avals.loading = false;
            state.avals.disabled = false;
            state.avals.payload = action.payload;
            return state;

        case DashboardActionTypes.SetDop:

            state.dops.selected = action.payload;

            state.avals.loading = true;
            state.avals.disabled = true;
            state.avals.selected = null;
            state.avals.payload = null;

            state.stores.loading = false;
            state.stores.disabled = true;
            state.stores.selected = null;
            state.stores.payload = null;

            state.collaborators.loading = false;
            state.collaborators.disabled = true;
            state.collaborators.selected = null;
            state.collaborators.payload = null;
            state.tests.loading = false;
            state.tests.disable = true;

            return state;

        case DashboardActionTypes.SetAval:

            state.avals.selected = action.payload;

            state.stores.loading = true;
            state.stores.disabled = true;
            state.stores.selected = null;
            state.stores.payload = null;

            state.collaborators.loading = false;
            state.collaborators.disabled = true;
            state.collaborators.selected = null;
            state.collaborators.payload = null;
            state.tests.loading = false;
            state.tests.disable = true;
            state.modal.tests = [[], [], [], [], []];

            return state;

        case DashboardActionTypes.DBStores:

            // change dops state

            state.stores.loading = false;
            state.stores.disabled = false;
            state.stores.payload = action.payload;
            return state;

        case DashboardActionTypes.SetStore:


            state.stores.selected = action.payload;
            state.collaborators.loading = true;
            state.collaborators.disabled = true;
            state.collaborators.selected = null;
            state.collaborators.payload = null;
            state.tests.loading = false;
            state.tests.disable = true;
            state.modal.tests = [[], [], [], [], []];

            return state;

        case DashboardActionTypes.SetCollaboratorsLoading:

            state.collaborators.loading = true;
            state.collaborators.disabled = true;
            state.modal.tests = [[], [], [], [], []];
            return state;


        case DashboardActionTypes.DBCollaborators:

            // change dops state

            state.collaborators.loading = false;
            state.collaborators.disabled = false;
            state.collaborators.payload = action.payload;
            return state;

        case DashboardActionTypes.SetCollaborator:

            // change dops state
            state.collaborators.selected = action.payload;
            state.tests.loading = true;
            state.tests.disable = true;
            state.modal.tests = [[], [], [], [], []];
            return state;

        case DashboardActionTypes.DBCollaboratorTests:

            // change dops state
            if (action.payload.length > 0) {
                state.tests.selected = action.payload[0].id;
                state.tests.payload[0].createdAt = action.payload[0].createdAt.toDate();


                state.tests.payload[0].test1 = action.payload[0].test1;

                if (action.payload[0].test1 !== undefined) {
                    state.modal.tests[0] = action.payload[0].test1.grades;
                }

                state.tests.payload[0].test2 = action.payload[0].test2;

                if (action.payload[0].test2 !== undefined) {
                    state.modal.tests[1] = action.payload[0].test2.grades;
                }

                state.tests.payload[0].test3 = action.payload[0].test3;

                if (action.payload[0].test3 !== undefined) {
                    state.modal.tests[2] = action.payload[0].test3.grades;
                }

                state.tests.payload[0].test4 = action.payload[0].test4;

                if (action.payload[0].test4 !== undefined) {
                    state.modal.tests[3] = action.payload[0].test4.grades;
                }

                state.tests.payload[0].test5 = action.payload[0].test5;

                if (action.payload[0].test5 !== undefined) {
                    state.modal.tests[4] = action.payload[0].test5.grades;
                }

            } else {

                state.tests.payload[0].createdAt = undefined;
                state.tests.selected = undefined;
                state.tests.payload[0].test1 = undefined;
                state.tests.payload[0].test2 = undefined;
                state.tests.payload[0].test3 = undefined;
                state.tests.payload[0].test4 = undefined;
                state.tests.payload[0].test5 = undefined;
            }

            state.tests.loading = false;
            state.tests.disable = false;

            return state;
        case DashboardActionTypes.ModalSetTestResult:
            state.modal.tests[action.payload.level][action.payload.index] = Number(action.payload.$event);
            return state;

        case DashboardActionTypes.UpdateTestResults:
            state.tests.loading = true;
            state.tests.disable = true;
            return state;

        case DashboardActionTypes.UpdateTestState:
            state.tests.complete = action.payload;

            return state;
        default:
            return state;

    }
}
