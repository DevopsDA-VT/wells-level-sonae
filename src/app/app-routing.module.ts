import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './auth/login/login.component';
import {LogoutComponent} from './auth/logout/logout.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/auth-guard';
import {UploadComponent} from './upload/upload.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);
const routes: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin},
    },
    {
        path: 'statistics',
        component: StatisticsComponent,
        canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin},
    },
    {
        path: 'upload',
        component: UploadComponent,
        canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin},
    },
    {
        path: 'auth',

        children: [{
            path: 'login',
            component: LoginComponent,
            pathMatch: 'full'
        }, {
            path: 'logout',
            component: LogoutComponent,
            pathMatch: 'full',
            canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin},
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
