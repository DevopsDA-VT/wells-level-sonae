import {Component} from '@angular/core';
import {AuthService} from './services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    items: any = [
        {
            title: 'Profile',
            icon: 'home',
            link: ['dashboard'],
        },
        {
            title: 'Statistics',
            icon: 'activity',
            link: ['statistics'],
        },
        {
            title: 'Update Data',
            icon: 'cloud-upload-outline',
            link: ['upload'],
        },
        {
            title: 'Logout',
            icon: 'log-out',
            link: ['auth/logout'],
        },
    ];

    constructor(public auth: AuthService) {

    }
}
