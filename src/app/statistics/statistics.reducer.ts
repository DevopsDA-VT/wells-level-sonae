import {ActionEx, StatisticsActions} from './statistics.actions';
import {StatisticsInterface} from '../models/statistics/statistics.interface';

export const initialState = new StatisticsInterface()
;


export function StatisticsReducer(state: StatisticsInterface = initialState, action: ActionEx) {
    switch (action.type) {

        case StatisticsActions.DBDops:
            state.dops.loading = false;
            state.dops.disabled = false;
            state.dops.payload = action.payload;
            return state;

        case StatisticsActions.SetDop:
            state.dops.selected = action.payload;
            state.store.loading = true;
            state.store.disabled = true;
            state.store.selected = null;
            state.store.payload = [];
            state.reviewer.displayName = '';
            state.reviewer.rank = 0;
            state.collaborators.payload = [];
            state.graph.storeperc = [];
            return state;

        case StatisticsActions.DBStores:
            state.store.loading = false;
            state.store.disabled = false;
            state.store.payload = action.payload;
            return state;

        case StatisticsActions.SetStore:

            state.store.selected = action.payload;
            state.collaborators.payload = [];
            return state;

        case StatisticsActions.SetReviewer:

            state.reviewer = action.payload;
            return state;

        case StatisticsActions.SetDopGraph:

            state.graph.dop = action.payload;
            return state;
        case StatisticsActions.SetStoreGraph:

            state.graph.store = action.payload;
            return state;
        case StatisticsActions.SetStorePerc:

            state.storeperc.payload.push(action.payload);
            return state;
        case StatisticsActions.SetStorePercGraph:

            state.graph.storeperc.payload = action.payload;
            return state;
        case StatisticsActions.SetCollaborators:

            state.collaborators.payload = action.payload;
            return state;

        default:
            return state;

    }
}
