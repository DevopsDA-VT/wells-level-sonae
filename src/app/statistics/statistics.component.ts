import {Component, HostListener, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {StatisticsInterface} from '../models/statistics/statistics.interface';
import {Observable} from 'rxjs';
import {StatisticsService} from '../services/statistics.service';
import {StatisticsDbDops, StatisticsDBStores, StatisticsSetCollaborators, StatisticsSetDop, StatisticsSetDopGraph, StatisticsSetStore, StatisticsSetStoreGraph} from './statistics.actions';
import {NbComponentStatus} from '@nebular/theme';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {StatisticsModalComponent} from './statistics-modal/statistics-modal.component';
import {SubTestsData, TestsData} from '../../assets/data/tests';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})

export class StatisticsComponent implements OnInit {

    innerWidth = window.innerWidth;

    statistics: Observable<StatisticsInterface>;
    statisticsState: StatisticsInterface = new StatisticsInterface();
    view: any = [];
    dopGraph = {
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: false,
        legendPosition: 'below',
        showXAxisLabel: true,
        xAxisLabel: '',
        xScaleMax: 5,
        showYAxisLabel: true,
        yAxisLabel: '',
        data: [],
        showGridLines: true,

    };
    storeGraph = {
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: false,
        legendPosition: 'below',
        showXAxisLabel: true,
        xAxisLabel: '',
        showYAxisLabel: true,
        yAxisLabel: '',
        data: [],
        showGridLines: true,
        YScaleMax: 5
    };
    storeGraphperc = {
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: false,
        legendPosition: 'below',
        showXAxisLabel: true,
        xAxisLabel: '',
        showYAxisLabel: true,
        yAxisLabel: '',
        showGridLines: true,
        data: [],
        YScaleMax: 100
    };
    collaboratorGraph = {
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: false,
        legendPosition: 'below',
        showXAxisLabel: true,
        xAxisLabel: 'TEST',
        xScaleMax: 5,
        showYAxisLabel: true,
        yAxisLabel: 'RANK',
        data: [],
        showGridLines: true,
        YScaleMax: 5

    };

    colorScheme = 'forest';
    view2D: any;
    Tests: any;
    SubTests: any;

    constructor(private statisticsService: StatisticsService,
                private store: Store<{ statistics: StatisticsInterface }>,
                private modalService: NgbModal) {
        this.Tests = TestsData;
        this.SubTests = SubTestsData;
        this.statistics = store.pipe(select('statistics'));


    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.setGraphView();


    }


    ngOnInit() {
        this.statisticsService.getDops().subscribe((QuerySnapshot: any) => {
            this.store.dispatch(new StatisticsDbDops(QuerySnapshot.sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            })));
            this.setDopsGraph(QuerySnapshot);
        });
        this.setGraphView();
        this.statistics.subscribe((state) => {
            this.statisticsState = state;
        });


    }

    onDopSelect(event) {
        this.setSelectDop(event.extra.dop.id);
    }

    setGraphView() {
        if (window.innerWidth <= 1199) {
            this.view = [window.innerWidth - window.innerWidth * 0.20, 400];
            this.view2D = [window.innerWidth - window.innerWidth * 0.10, 400];
        } else if (1200 <= window.innerWidth && window.innerWidth <= 1366) {
            this.view = [window.innerWidth - window.innerWidth * 0.81, 400];
            this.view2D = [window.innerWidth - window.innerWidth * 0.41, 400];
        } else if (1367 <= window.innerWidth && window.innerWidth <= 1919) {
            this.view = [window.innerWidth - window.innerWidth * 0.80, 400];
            this.view2D = [window.innerWidth - window.innerWidth * 0.41, 400];
        } else {
            this.view = [550, 400];
            this.view2D = [1100, 400];
        }
    }

    getGraphView() {
        return this.view;
    }

    get2DGraphView() {
        return this.view2D;
    }

    setSelectDop($event: any[] | any) {
        this.store.dispatch(new StatisticsSetDop($event));
        this.statisticsState.dops.selected = $event;
        this.statisticsService.getDBStores($event).subscribe((ZonePromise) => {
            let storeArray = [];
            const StorePercArray = [];

            ZonePromise.then((Querysnapshots) => {

                for (const Querysnapshot of Querysnapshots) {

                    Querysnapshot.data.subscribe((docs) => {

                        docs.forEach(doc => {
                            const data = doc.data();
                            data.parent = Querysnapshot.parent;
                            data.id = doc.id;
                            storeArray.push(data);
                        });
                        storeArray = storeArray.sort((a, b) => {
                            if (a.displayName < b.displayName) {
                                return -1;
                            }
                            if (a.displayName > b.displayName) {
                                return 1;
                            }
                            return 0;
                        });
                        this.store.dispatch(new StatisticsDBStores(storeArray));

                        this.setStoreGraph(storeArray);
                    });
                }

            });
        });
    }

    getDopsStateDisable() {
        return this.store.select(state => state.statistics.dops.disabled);
    }

    getDopsStateLoading() {
        return this.store.select(state => state.statistics.dops.loading);
    }

    getSelectedDop() {
        return this.store.select(state => state.statistics.dops.selected);
    }

    getDops() {
        let dops = [];
        this.statistics.subscribe((state) => {
            dops = state.dops.payload;
        });
        return dops;
    }

    setSelectStore($event: any[] | any) {
        this.store.dispatch(new StatisticsSetStore($event));
        this.statisticsState.store.selected = $event;
        this.store.select(state => state.statistics).subscribe((statistics) => {
            const storeData: any = statistics.store.payload.filter((element: any) => {
                return element.id === $event;
            });

            this.statisticsService.getCollaborator(statistics.dops.selected, storeData[0].parent, $event).subscribe((collaborators) => {
                this.store.dispatch(new StatisticsSetCollaborators(collaborators));
                this.statistics.subscribe((res: StatisticsInterface) => {
                    this.statisticsState.collaborators = res.collaborators;

                });
            });
        });
    }

    getStoreStateLoading() {
        return this.store.select(state => state.statistics.store.loading);
    }

    getSelectedStore() {
        return this.store.select(state => state.statistics.store.selected);
    }

    getStoreStateDisable() {
        return this.store.select(state => state.statistics.store.disabled);
    }

    getStores() {
        let stores = [];
        this.statistics.subscribe((state) => {
            this.statisticsState.store = state.store;
            stores = state.store.payload;
        });
        return stores;
    }

    getGlobalRank() {
        let rank = 0;
        this.statistics.subscribe((state) => {
            rank = this.statisticsService.getGlobalDopRank(state.dops);
        });
        return rank;
    }

    getDopsRank() {
        let rank = 0;
        this.statistics.subscribe((state) => {
            rank = this.statisticsService.getDopRank(state.store);
        });
        return rank;
    }

    getStoreRank() {
        let rank = 0;
        let store = null;

        this.statistics.subscribe((state) => {
            if (state.store.selected) {
                store = state.store.payload.filter((entry: any) => {
                    return entry.id === state.store.selected;
                });
                if (store !== null) {
                    rank = store[0].rank;
                    if (rank === null) {
                        rank = 0;
                    }
                }

            }
        });
        return rank;
    }

    getCollaborators() {

        let collabData = [];
        if (this.statisticsState.collaborators.payload.length > 0) {
            collabData = this.statisticsState.collaborators.payload.sort((a: any, b: any) => {
                if (a.displayName < b.displayName) {
                    return -1;
                }
                if (a.displayName > b.displayName) {
                    return 1;
                }
                return 0;
            });

        }

        return collabData;
    }

    getCollaboratorsStateLoading() {
        return this.store.select(state => state.statistics.collaborators.loading);
    }

    getReviewerName() {
        return this.store.select(state => state.statistics.reviewer.displayName);
    }

    getReviewerRank() {
        return this.store.select(state => state.statistics.reviewer.rank);
    }

    getDopGraphData() {
        return this.store.select(state => state.statistics.graph.dop);
    }

    setDopsGraph(QuerySnapshot: any) {
        const dops: any = QuerySnapshot;
        let data = [];
        for (const dop of dops) {
            let rank;
            if (!dop.rank) {
                rank = 0;
            } else {
                rank = dop.rank;
            }
            const dopData = {
                name: dop.dop,
                value: rank,
                extra:
                    {
                        dop
                    }

            };
            data.push(dopData);
        }
        data = data.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        });
        this.store.dispatch(new StatisticsSetDopGraph(data));
    }

    getStoreGraphData() {
        return this.store.select(state => state.statistics.graph.store);
    }

    getStoreGraphPercData() {
        return this.store.select(state => state.statistics.graph.storeperc);
    }

    onStoreSelect(event) {
        this.setSelectStore(event.extra.store.id);
    }

    setStoreGraph(storeArray: any[]) {
        const stores: any = storeArray;
        let data = [];
        for (const store of stores) {
            let rank;
            if (!store.rank) {
                rank = 0;
            } else {
                rank = store.rank;
            }
            const StoreData = {
                name: store.displayName,
                value: rank,
                extra: {
                    store
                }
            };
            data.push(StoreData);
        }
        data = data.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        });

        this.store.dispatch(new StatisticsSetStoreGraph(data));
    }


    getStatus(id: string, index: number): NbComponentStatus {
        const collab = this.statisticsState.collaborators.payload.filter((collaborator: any) => collaborator.id === id)[0];
        try {
            if (collab[`test${index}`] !== undefined) {
                const grade = collab[`test${index}`].finalGrade;
                if (grade >= 3) {
                    return 'success';
                } else if (grade >= 0 && grade < 3) {
                    return 'danger';
                } else {
                    return 'primary';
                }
            } else {
                return 'primary';
            }
        } catch (e) {
            console.log(e);
        }


    }

    getStatusDisable(id: string, index: number) {
        const collab = this.statisticsState.collaborators.payload.filter((collaborator: any) => collaborator.id === id)[0];
        try {

            return collab[`test${index}`] === undefined;
        } catch (e) {
            console.log(e);
        }
    }

    open(id: string, index: number) {
        // const modalRef = this.modalService.open(ModalComponent);
        const modalRef = this.modalService.open(StatisticsModalComponent);
        modalRef.componentInstance.title = this.Tests[index];
        modalRef.componentInstance.tests = this.SubTests[index];
        modalRef.componentInstance.grades = this.statisticsState.collaborators.payload.filter((collab: any) => collab.id === id)[0][`test${index + 1}`];
    }
}
