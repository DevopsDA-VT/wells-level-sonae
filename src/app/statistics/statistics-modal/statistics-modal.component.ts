import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-statistics-modal',
    templateUrl: './statistics-modal.component.html',
    styleUrls: ['./statistics-modal.component.scss']
})
export class StatisticsModalComponent implements OnInit {

    @Input() title = `Information`;
    @Input() tests;
    @Input() grades: any;
    count = 0;

    constructor(
        public activeModal: NgbActiveModal
    ) {
    }

    ngOnInit() {
    }


}
