import {Action} from '@ngrx/store';

export enum StatisticsActions {
    DBDops = '[Statistics Component] DB Get Dops',
    DBStores = '[Statistics Component] DB Get Stores',
    SetDop = '[Statistics Component] Set Dop',
    SetStore = '[Statistics Component] Set Store',
    SetStorePerc = '[Statistics Component] Set Store perc',
    SetReviewer = '[Statistics Component] Set Reviewer',
    SetCollaborators = '[Statistics Component] Set Collaborators',
    SetDopGraph = '[Statistics Component] Set Dop Graph Data',
    SetStoreGraph = '[Statistics Component] Set Store Graph Data',
    SetStorePercGraph = '[Statistics Component] Set Store Perc Graph Data',
}

export class ActionEx implements Action {
    readonly type;
    payload: any;
}

export class StatisticsDbDops implements ActionEx {
    readonly type = StatisticsActions.DBDops;

    constructor(public payload: any) {
    }
}

export class StatisticsSetDop implements ActionEx {
    readonly type = StatisticsActions.SetDop;

    constructor(public payload: any) {
    }
}

export class StatisticsDBStores implements ActionEx {
    readonly type = StatisticsActions.DBStores;

    constructor(public payload: any) {
    }
}

export class StatisticsSetStore implements ActionEx {
    readonly type = StatisticsActions.SetStore;

    constructor(public payload: any) {
    }
}

export class StatisticsSetReviewer implements ActionEx {
    readonly type = StatisticsActions.SetReviewer;

    constructor(public payload: any) {
    }
}

export class StatisticsSetDopGraph implements ActionEx {
    readonly type = StatisticsActions.SetDopGraph;

    constructor(public payload: any) {
    }
}

export class StatisticsSetStoreGraph implements ActionEx {
    readonly type = StatisticsActions.SetStoreGraph;

    constructor(public payload: any) {
    }
}

export class StatisticsSetCollaborators implements ActionEx {
    readonly type = StatisticsActions.SetCollaborators;

    constructor(public payload: any) {
    }
}

export class StatisticsDBStoresPerc implements ActionEx {
    readonly type = StatisticsActions.SetStorePerc;

    constructor(public payload: any) {
    }
}

export class StatisticsSetStorePercGraph implements ActionEx {
    readonly type = StatisticsActions.SetStorePercGraph;

    constructor(public payload: any) {
    }
}



