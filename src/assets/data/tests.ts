export const TestsData = ['1. Lead', '2. Enquire', '3. Value', '4. Engage', '5. Level Up'];
export const SubTestsData =
    [
        [
            {index: 0, name: 'O colaborador sabe descrever os 4 objetivos desta etapa'},
            {index: 1, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem domínio das técnicas lead na placa de vendas.'},
            {index: 2, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem domínio das técnicas lead no espaço da loja'},
            {index: 3, name: 'O colaborador demonstra consistentemente comportamentos  de orientação para o cliente e para a venda através de uma escuta ativa'}
        ],
        [
            {index: 0, name: 'O colaborador sabe descrever quais os 5 objetivos desta etapa'},
            {index: 1, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem a correta REFORMULAÇÃO POSITIVA das necessidades do cliente'},
            {index: 2, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem o correto DIAGNÓSTICO das necessidades do cliente'},
            {index: 3, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem a ANTECIPAÇÃO DAS NECESSIDADES E EXPECTATIVAS FUTURAS do cliente, aplicando correntemente a técnica do FOCO LONGITUDINAL 360º'},
            {index: 4, name: 'O colaborador demonstra consistentemente comportamentos  que demonstram o CONTROLO DA INTERAÇÃO COMERCIAL através da conjugação das técnicas comunicacionais'},
            {index: 5, name: 'O colaborador demonstra consistentemente um discurso ORIENTADO PARA A SOLUÇÃO utilizando corretamente a LINGUAGEM POSITIVA'}
        ], [
        {index: 0, name: 'O colaborador sabe descrever quais os 5 objetivos desta etapa'},
        {index: 1, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem a APRESENTAÇÃO DA SOLUÇÃO DE FORMA ESTRUTURADA utilizando a técnica FBI+R'},
        {index: 2, name: 'O colaborador demonstra consistentemente comportamentos que ajudam a PROMOVER A EXPERIMENTAÇÃO em loja dominando a técnica de MARKETING EXPERIMENTAL'},
        {index: 3, name: 'O colaborador demonstra consistentemente comportamentos  que promovem o UP SELLING e o CROSS SELLING explorando necessidades latentes'},
        {index: 4, name: 'O colaborador demonstra consistentemente comportamentos  que aumentam a CONFIANÇA DO CLIENTE articulando corretamente a LINGUAGEM ESPECIALISTA'},
    ],
        [
            {index: 0, name: 'O colaborador sabe descrever quais os 4 objetivos desta etapa'},
            {index: 1, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem a INFLUÊNCIA E PERSUASÃO na relação com o cliente\n'},
        ],
        [
            {index: 0, name: 'O colaborador sabe descrever quais os 5 objetivos desta etapa'},
            {index: 1, name: 'O colaborador demonstra consistentemente comportamentos  que traduzem o compromisso do cliente com uma PRÓXIMA VISITA\n'},
            {index: 2, name: 'O colaborador demonstra consistentemente comportamentos  que estimulam o cliente a PARTILHAR A SUA EXPERIÊNCIA POSITIVA'},
            {index: 3, name: 'O colaborador demonstra consistentemente comportamentos  de DIVULGAÇÃO DE CAMPANHAS E ENTREGA DE FOLHETOS\n'},
            {index: 4, name: 'O colaborador demonstra consistentemente comportamentos que traduzem o foco na SARTISFAÇÃO E SUPERAÇÃO DAS EXPECTATIVAS do cliente\n  '},
        ]
    ];
